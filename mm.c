/* 
 * mm-handout.c -  Simple allocator based on explicit free lists and 
 *                 first fit placement (similar to lecture5.pptx). 
 *                 It uses boundary tags and performs
 *                 coalescing. Realloc, if given a size increase, 
 * 								 always allocates a new block of data and copies over the old data. 
 * 								 Thus, this gives good performance for everything BUT the realloc traces. 
 *
 * Each block has a header/footer of the form:
 * 
 *      31                     3  2  1  0 
 *      -----------------------------------
 *     | s  s  s  s  ... s  s  s  0  0  a/f
 *      ----------------------------------- 
 * 
 * where s are the meaningful size bits and a/f is set 
 * iff the block is allocated. 
 * 
 * In addition, each free block has at the beginning of the payload block
 * TWO pointers to the next free block and previous free block, respectively.
 * If at the end or beginning of the list block the next and previous pointers will 
 * be NULL respectively.
 * 
 * The list has the following form:
 *
 * begin                                                         end
 * heap                                                          heap  
 *  -----------------------------------------------------------------   
 * |  pad   | hdr(8:a) |   ftr(8:a)   | zero or more usr blks | hdr(8:a) |
 *  -----------------------------------------------------------------
 *    four  | prologue |   prologue   |                       | epilogue |
 *    bytes | block    |   footer     |                       | block    |
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "mm.h"
#include "memlib.h"


/* Team structure */
team_t team = {
	/* Team name */
	"Alex-Team",
	/* note that we will add a 10% bonus for
	* working alone */
	/* the maximum number of members per team
	 * is four */
	/* First member's full name */
	"Alex Richardson",
	/* First member's email address */
	"alexrichardsonschool@gmail.com",
	/* Second member's full name (leave
	* blank if none) */
	"",
	/* Second member's email address
	* (leave blank if none) */
	""
};


/* $begin mallocmacros */
/* Basic constants and macros */

/* You can add more macros and constants in this section */
#define WSIZE       4       /* word size (bytes) */  
#define DSIZE       8       /* doubleword size (bytes) */
#define MIN_SIZE		(4 * DSIZE) /* Minimum allowed size of block */
#define CHUNKSIZE  (1<<12)  /* initial heap size (4 kilobytes) */
#define OVERHEAD    8       /* overhead of header and footer (8 bytes) */

#define MAX_FREE_LIST_SIZE (8192*4)
#define MAX(x, y) ((x) > (y)? (x) : (y))  

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc)  ((size) | (alloc))

/* Read and write a word at address p */
#define GET(p)       (*(size_t *)(p))
#define PUT(p, val)  (*(size_t *)(p) = (val))  

/*Read/set the next and previous fields from address bp */
#define GET_NEXT(bp) (*(void**)(bp))
#define GET_PREV(bp) (*(void**)((char*)(bp) + WSIZE))
#define SET_NEXT(bp, next) (PUT(bp, (size_t)next))
#define SET_PREV(bp, prev) (PUT(((char*)(bp) + WSIZE), (size_t)prev))
/* Read/set the size and allocated fields from address p */
#define GET_SIZE(p)  		(GET(p) & ~0x7)
#define GET_ALLOC(p) 		(GET(p) & 0x1)
#define CLEAR_ALLOC(p) 	(PUT(p, GET_SIZE(p)))
#define SET_ALLOC(p) 			(PUT(p, GET_SIZE(p) | 1))

/* Given block ptr bp, compute address of its header*/
#define HDRP(bp)       ((char *)(bp) - WSIZE)
/* Given block ptr bp, compute address of its footer*/
#define FTRP(bp)			 (NEXT_BLKP(bp) - DSIZE)   

/* Given block ptr bp, compute address of previous block */
#define PREV_BLKP(bp)	 ((char *)(bp) - GET_SIZE(((char*)(bp) - DSIZE)))
/* Given block ptr bp, compute address of next block */
#define NEXT_BLKP(bp)  ((char *)(bp) + GET_SIZE(((char*)(bp) - WSIZE)))

/* $end mallocmacros */


/* Global variables */
static char *heap_listp;  /* pointer to first block */  
static char *heap_free_listp[MAX_FREE_LIST_SIZE]; /* Pointer to first entry in heap_free_list */
static size_t heap_free_list_max = 0;

/* function prototypes for internal helper routines */
static void *extend_heap(size_t words);
static void place(void *bp, size_t asize, int free);
static void *best_fit(size_t asize);
static void *find_fit(char* free_list, size_t asize);
static void printblock(void *bp);
static size_t compute_free_list_index(size_t size);
static void add_to_free_list(void* bp);
static void remove_from_free_list(void* bp);

static size_t compute_free_list_index(size_t size) { 
	if (size <= 16) { 
		return 0;
	}
	else if (size <= MAX_FREE_LIST_SIZE) { 
		if (size % 2) { 
			size--;
		}
		return (size - 16) / 2;
	}
	else { 
		return (compute_free_list_index(MAX_FREE_LIST_SIZE) + 1);
	}
}

/* 
 * mm_init - Initialize the memory manager 
 */
/* $begin mminit */
int mm_init(void) 
{
	 //puts("Initializing....");
	  
   heap_free_list_max =  compute_free_list_index(MAX_FREE_LIST_SIZE) + 1;
   /* create the initial empty heap */
   for (size_t i = 0; i < MAX_FREE_LIST_SIZE; i++) { 
	   heap_free_listp[i] = NULL;
   }
   if ((heap_listp = mem_sbrk(4*WSIZE)) == NULL)
		return -1;
   PUT(heap_listp, KEY);               /* alignment padding */
   PUT(heap_listp+WSIZE, PACK(DSIZE, 0));  /* prologue header */ 
   PUT(heap_listp+DSIZE, PACK(DSIZE, 0));  /* prologue footer*/ 
   PUT(heap_listp+DSIZE+WSIZE, PACK(0, 0));   /* epilogue header */
   heap_listp += (DSIZE);

   /* Extend the empty heap with a free block of CHUNKSIZE bytes */
   if (extend_heap(CHUNKSIZE/WSIZE) == NULL)
		return -1;
   return (int) heap_listp;
}
/* $end mminit */

/* 
 * mm_malloc - Allocate a block with at least size bytes of payload 
 */
/* $begin mmmalloc */
void *mm_malloc(size_t size) 
{
    size_t asize;      /* adjusted block size */
    size_t extendsize; /* amount to extend heap if no fit */
    char *bp;      
	//	printf("call mm_malloc\n");

    /* Ignore spurious requests */
    if (size <= 0)
		return NULL;

    /* Adjust block size to include overhead and alignment reqs. */
    if (size <= MIN_SIZE)
		asize = MIN_SIZE;
    else
		asize = DSIZE * ((size + (OVERHEAD) + (DSIZE-1)) / DSIZE);
    
    /* Search the free list for a fit */
    if ((bp = best_fit(asize)) != NULL) {
		place(bp, asize, 1);
		return bp;
    }

    /* No fit found. Get more memory and place the block */
    extendsize = MAX(asize,CHUNKSIZE);
    if ((bp = extend_heap(extendsize/WSIZE)) == NULL)
	 {
	 	printf("mm_malloc = NULL\n");
		return NULL;
	 }
    place(bp, asize,1);
    return bp;
} 
/* $end mmmalloc */

/* 
 * mm_free_unify_left - 
 * Coalesces a block leftward up to the next allocated block or beginning of heap, 
 * whichever comes first.
 */
static void* mm_unify_left(void* bp, int free) { 
	char* nblkp = PREV_BLKP(bp);
	char* hdr_nblkp = HDRP(nblkp); 
	char* hdr_blkp = HDRP(bp);
	while (GET_ALLOC(hdr_nblkp)) {
		if (free) { 
			remove_from_free_list(bp);
		}
		remove_from_free_list(nblkp);
		size_t cur_size = GET_SIZE(hdr_blkp);
		size_t next_size = GET_SIZE(hdr_nblkp);
		PUT(hdr_nblkp, PACK(cur_size + next_size, free));
		PUT(FTRP(nblkp), PACK(cur_size + next_size, free));
		bp = nblkp;
		nblkp = PREV_BLKP(bp);
		hdr_nblkp = HDRP(nblkp); 
		hdr_blkp = HDRP(bp);
	}
	return bp;
}

/* 
 * mm_free_unify_right - 
 * Coalesces a block rightward up to the next allocated block or end of heap, 
 * whichever comes first.
 */
static void* mm_unify_right(void* bp, int free) {
	char* nblkp = NEXT_BLKP(bp);
	char* hdr_nblkp = HDRP(nblkp); 
	char* hdr_blkp = HDRP(bp);
	while (GET_ALLOC(hdr_nblkp)) {
		if (free) { 
			remove_from_free_list(bp);
		}
		remove_from_free_list(nblkp);
		size_t cur_size = GET_SIZE(hdr_blkp);
		size_t next_size = GET_SIZE(hdr_nblkp);
		PUT(hdr_blkp, PACK(cur_size + next_size, free));
		PUT(FTRP(bp), PACK(cur_size + next_size, free));
		nblkp = NEXT_BLKP(bp);
		hdr_nblkp = HDRP(nblkp); 
		hdr_blkp = HDRP(bp);
	}
	return bp;
}

/* 
 * add_to_free_list - Add a block to free list
 */
static void add_to_free_list(void* bp) {
	size_t size = GET_SIZE(HDRP(bp));
	size_t index = compute_free_list_index(size);
	char* listp = heap_free_listp[index];
	void* insert_point_prev = NULL;
	void* insert_point_next = listp;
	for (; (insert_point_next != NULL) && (insert_point_next < bp); insert_point_prev = insert_point_next, insert_point_next = GET_NEXT(insert_point_next));
	if  (insert_point_next == bp) { 
		mm_checkheap(1);
		exit(1);
	}
	if (insert_point_next != NULL) {
		SET_PREV(insert_point_next, bp);
	}
	if (insert_point_prev != NULL) { 
		SET_NEXT(insert_point_prev, bp);
	}
	SET_NEXT(bp, insert_point_next);
	SET_PREV(bp, insert_point_prev);
	if (insert_point_prev == NULL) { 
		heap_free_listp[index] = bp;
	}
}

/* 
 * remove_to_free_list - Remove a block from free list
 */
static void remove_from_free_list(void* bp) { 
	size_t size = GET_SIZE(HDRP(bp));
	size_t index = compute_free_list_index(size);
	char* next = GET_NEXT(bp);
	char* prev = GET_PREV(bp);
	if (next != NULL) { 
		SET_PREV(next, prev);
	}
	if (prev != NULL) { 
		SET_NEXT(prev, next);
	}
	else { 
		heap_free_listp[index] = next;
	}
}
 
/* 
 * mm_free - Free a block 
 */
/* $begin mmfree */
void mm_free(void *bp)
{
	bp = mm_unify_right(mm_unify_left(bp, 0), 0);
	SET_ALLOC(HDRP(bp));
	add_to_free_list(bp);
}

/* $end mmfree */

/*
 * mm_realloc - implementation of mm_realloc
 * It either shrinks the existing block, unifies it with other blocks to get more space, or 
 * just allocates a whole new block if necessary
 */
void *mm_realloc(void *ptr, size_t size)
{
	size_t asize = DSIZE * ((size + (OVERHEAD) + (DSIZE-1)) / DSIZE);
	void* res = NULL;
	if (ptr != NULL) {
		size_t orig_data_size = GET_SIZE(HDRP(ptr)) - DSIZE;
		size_t cur_size = GET_SIZE(HDRP(ptr));
		if (cur_size >= asize) { 
			res = ptr;
			place(ptr, asize, 0);
		}
		else { 
			void* old_ptr = ptr;
			mm_unify_right(ptr, 0);
			cur_size = GET_SIZE(HDRP(ptr));
			if (cur_size < asize) {  
				res = mm_malloc(size);
				memmove(res, old_ptr, orig_data_size);
				mm_free(ptr);
			}
			else { 
				res = ptr;
				memmove(res, old_ptr, orig_data_size);
			}
		}
	}
	else {
		res = mm_malloc(size);
	}
	return res;
}


/* 
 * mm_checkheap - Check the heap for consistency 
 */
void mm_checkheap(int verbose) 
{
	char *bp = heap_listp;
	puts("--------------------");
	puts("Heap block");
	if (verbose)
		printf("Heap (%p):\n", heap_listp);
	if ((GET_SIZE(HDRP(heap_listp)) != DSIZE) || GET_ALLOC(HDRP(heap_listp))) {
		printf("Bad prologue header\n");
		fflush(stdout);
		exit(1);
	}

	for (bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
		if (verbose)
			printblock(bp);
	}

	if (verbose)
		printblock(bp);
	if ((GET_SIZE(HDRP(bp)) != 0) || (GET_ALLOC(HDRP(bp)))) {
		printf("Bad epilogue header\n");
		fflush(stdout);
		exit(1);
	}
	puts("--------------------");

	puts("--------------------");
	puts("Free lists");
	for (int i = 0; i < MAX_FREE_LIST_SIZE; i++) { 
		for (bp = heap_free_listp[i]; bp != NULL; bp = GET_NEXT(bp)) {
			if (verbose)
				printblock(bp);
			if (compute_free_list_index(GET_SIZE(HDRP(bp))) != i) { 
				puts("Not in proper segregated list location");
				fflush(stdout);
				exit(1);
			}
		}
	}
	fflush(stdout);
}

/* The remaining routines are internal helper routines */

/* 
 * extend_heap - Extend heap with free block and return its block pointer
 */
/* $begin mmextendheap */
static void *extend_heap(size_t words) 
{
    char *bp;
    size_t size;
	
    /* Allocate an even number of words to maintain alignment */
    size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
    if ((bp = mem_sbrk(size)) == (void *)-1) 
		return NULL;

    /* Initialize free block header and the epilogue header */
    PUT(HDRP(bp), PACK(size, 1));         /* free block header */
		PUT(FTRP(bp), PACK(size, 1));					/* free block footer */
		add_to_free_list(bp);
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 0)); /* new epilogue header */
    return bp;
}
/* $end mmextendheap */

/* 
 * place - Place block of asize bytes at start of free block bp 
 *         and split if remainder would be at least minimum block size
 */
/* $begin mmplace */
/* $begin mmplace-proto */
static void place(void *bp, size_t asize, int free)
/* $end mmplace-proto */
{
    size_t csize = GET_SIZE(HDRP(bp));   
    if (free) {
        remove_from_free_list(bp);
    }
    if ((csize - asize) >= MIN_SIZE) { 
        PUT(HDRP(bp), PACK(asize, 0));
        PUT(FTRP(bp), PACK(asize, 0));
        bp = NEXT_BLKP(bp);
        PUT(HDRP(bp), PACK(csize-asize, 1));
        PUT(FTRP(bp), PACK(csize-asize, 1));
        add_to_free_list(bp);
    }
    else { 
        PUT(HDRP(bp), PACK(csize, 0));
        PUT(FTRP(bp), PACK(csize, 0));
    }
}
/* $end mmplace */

/* 
 * best_fit - Find a best fit for a block with asize bytes from all the segregated free lists
 */
static void *best_fit(size_t asize) { 
	for (size_t i = compute_free_list_index(asize); i <= heap_free_list_max; i++) { 
		void* res = find_fit(heap_free_listp[i], asize);
		if (res != NULL) { 
			return res;
		}
	}
	return NULL;
}

/* 
 * find_fit - Find a fit for a block with asize bytes from a given segregated list
 */
static void *find_fit(char* free_list, size_t asize)
{
    /* first fit search */
    void *bp;

    for (bp = free_list; bp != NULL; bp = GET_NEXT(bp)) {
			if (GET_ALLOC(HDRP(bp)) && (asize <= GET_SIZE(HDRP(bp)))) {
					return bp;
			}
    }
    return NULL; /* no fit */
}
/* 
 *  printblock - Print block info
 */
static void printblock(void *bp)
{
	    size_t hsize, fsize, halloc;

		hsize = GET_SIZE(HDRP(bp));
		fsize = GET_SIZE(FTRP(bp));
		halloc = GET_ALLOC(HDRP(bp));
		
		if (hsize == 0) {
			printf("%p: EOL\n", bp);
			return;
		}

		printf("%p: header: [%zu:%zu:%c] [%p:%p]\n", bp, hsize, fsize,  (halloc ? 'f' : 'a'), GET_NEXT(bp), GET_PREV(bp));
		if ((hsize != fsize) || ((hsize % 8) != 0)) { 
			puts("WTF?!?!?");
			fflush(stdout);
			exit(1);
		}
}
